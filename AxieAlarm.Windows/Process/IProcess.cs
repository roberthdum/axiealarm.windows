﻿using AxieAlarm.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AxieAlarm.Windows.Process
{
    public interface IProcess
    {
        Task<AxiesFiltredEntitie> GetAxieFiltred(Dictionary<string, object> customer);
    }
}