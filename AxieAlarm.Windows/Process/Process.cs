﻿using AxieAlarm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AxieAlarm.MapsTemplates;
using AxieAlarm.Generic;
using AxieAlarm.ApiRest;
using System.Net.Http;
using AxieAlarm.Data.ViewModels;

namespace AxieAlarm.Windows.Process
{
    public class Process : IProcess
    {
        private ICustomMapping _customMapping;
        private IConfigurationsEndpoints _configurations;
        private ResSharp _resSharp;

        public Process()
        {
            _customMapping = new JsonCustomMapping();
            _resSharp = new ResSharp();
            _configurations = new ConfigurationsEndpoints();
        }

        public async Task<AxiesFiltredEntitie> GetAxieFiltred(Dictionary<string, object> customer)
        {


            var data = new Dictionary<string, object>();
            var request2 = _customMapping.Maptemplates(customer, MapJsonType.AxieFIltred);

            var request = _customMapping.Maptemplates< RequestFiltresViewModel>(customer, MapJsonType.AxieFIltred);
            var endpoint = _configurations.GetEndpoint("Axie");
            endpoint.endpointName = "graphql";
            var result = await _resSharp.RequestAsync<AxiesFiltredEntitie>(endpoint, request, HttpMethod.Post, "", null, false);


            return result;
        }
    }
}
