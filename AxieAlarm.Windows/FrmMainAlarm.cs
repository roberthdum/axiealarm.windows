﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AxieAlarm.Configuration;
using AxieAlarm.Data.Entities;
using AxieAlarm.Data.ViewModels;
using AxieAlarm.Generic;
using pro = AxieAlarm.Windows.Process;

namespace AxieAlarm.Windows
{
    public partial class AxieAlarm : Form
    {
        pro.IProcess _procees;
     public int number
        {
            get;set;
        }
        public List<AxieResult> AxieNotifiqued { get; set; }
        public AxieAlarm()
        {
            InitializeComponent();
            _procees = new pro.Process();
            result.AutoGenerateColumns = false;
        }
        RequestFiltresViewModel _request;
        public RequestFiltresViewModel Request
        {


            get
            {
                if (_request == null)
                {
                    initobjaxie();

                }

                if (_health01.Value != 27 || _health02.Value != 61)
                    _request.variables.criteria.hp = new List<int>() { _health01.Value.ToObject<int>(), _health02.Value.ToObject<int>() };

                if (_morale01.Value != 27 || _morale02.Value != 61)

                    _request.variables.criteria.morale = new List<int>() { _morale01.Value.ToObject<int>(), _morale02.Value.ToObject<int>() };

                if (_speed01.Value != 27 || _speed02.Value != 61)

                    _request.variables.criteria.speed = new List<int>() { _speed01.Value.ToObject<int>(), _speed02.Value.ToObject<int>() };
                if (_skill01.Value != 27 || _skill02.Value != 61)

                    _request.variables.criteria.skill = new List<int>() { _skill01.Value.ToObject<int>(), _skill02.Value.ToObject<int>() };
                if (_breed01.Value != 0 || _breed02.Value != 7)

                    _request.variables.criteria.breedCount = new List<int>() { _breed01.Value.ToObject<int>(), _breed02.Value.ToObject<int>() }; if (_morale01.Value != 27 || _morale02.Value != 61)
                    if (_purenes01.Value != 0 || _purenes01.Value != 7)

                        _request.variables.criteria.pureness = new List<int>() { _purenes01.Value.ToObject<int>(), _purenes02.Value.ToObject<int>() };

                return _request;
            }

            set
            {

                _request = value;

                if (_request.variables != null)
                {

                    if (_request.variables.criteria.hp != null)
                    {
                        _health01.Value = _request.variables.criteria.hp[0].ToObject<int>();
                        _health02.Value = _request.variables.criteria.hp[1].ToObject<int>();
                    }
                    if (_request.variables.criteria.morale != null)
                    {
                        _morale01.Value = _request.variables.criteria.morale[0].ToObject<int>();
                        _morale02.Value = _request.variables.criteria.morale[1].ToObject<int>();
                    }

                    if (_request.variables.criteria.skill != null)
                    {

                        _skill01.Value = _request.variables.criteria.skill[0].ToObject<int>();
                        _skill02.Value = _request.variables.criteria.skill[1].ToObject<int>();
                    }
                    if (_request.variables.criteria.speed != null)
                    {
                        _speed01.Value = _request.variables.criteria.speed[0].ToObject<int>();
                        _speed02.Value = _request.variables.criteria.speed[1].ToObject<int>();
                    }
                    if (_request.variables.criteria.breedCount != null)
                    {
                        _breed01.Value = _request.variables.criteria.breedCount[0].ToObject<int>();
                        _breed02.Value = _request.variables.criteria.breedCount[1].ToObject<int>();
                    }
                    if (_request.variables.criteria.pureness != null)
                    {
                        _purenes01.Value = _request.variables.criteria.pureness[0].ToObject<int>();
                        _purenes02.Value = _request.variables.criteria.pureness[1].ToObject<int>();
                    }


                    cheked(Beast, _request);
                    cheked(Aquatic, _request);
                    cheked(Reptile, _request);
                    cheked(Bird, _request);
                    cheked(Bug, _request);
                    cheked(Mech, _request);
                    cheked(Dusk, _request);
                    cheked(Dawn, _request);


                    selected.Items.AddRange(Request.variables.criteria.parts.ToArray());

                }


            }
        }

        private void initobjaxie()
        {
            _request = new RequestFiltresViewModel();
            _request.variables = new Variables();
            _request.variables.criteria = new Criteria();
            _request.variables.criteria.classes = new List<string>();
            _request.variables.criteria.parts = new List<string>();
        }

        void cheked(CheckBox che, RequestFiltresViewModel _request)
        {
            che.Checked = _request.variables.criteria.classes.FirstOrDefault(x => x.Contains(che.Name)) != null;
        }
        private async void Form1_LoadAsync(object sender, EventArgs e)
        {
            FiltredBodyPart();

            try
            {
                var fileaxie = $"Data/filtred{number}.json".GetFile().ToConvertObjects<RequestCustomFiltresViewModel>();
                _fromusd01.Text = fileaxie.pricemin;
                _tosd01.Text = fileaxie.pricemax;

                Request = ((RequestFiltresViewModel)fileaxie);
            }
            catch { }
        }

        public void Alert(string msg, Form_Alert.enmType type, Result axie, bool clickmio = false)
        {
            Form_Alert frm = new Form_Alert();
            if (clickmio)
            {
                frm.Clickmio += Frm_Clickmio;
            }
            frm.showAlert(msg, type, axie);
        }

        private void Frm_Clickmio(Result axie)
        {
            OpenAxie(axie);
        }

        private static void OpenAxie(Result axie)
        {
            if (axie != null)
            {
                System.Diagnostics.Process.Start(axie.url);

            }
        }

        private async Task Filtred()
        {


            var tes = await _procees.GetAxieFiltred((Request.ToDictionary()));
            result.DataSource = tes.data.axies.results;
        }

        private void FiltredBodyPart(string name = "")
        {
            var list = "Data/bodypart.json".GetFile();

            var bdParts = new List<CustomPart>();

            if (!string.IsNullOrEmpty(name))
            {
                bdParts = list.ToConvertObjects<List<CustomPart>>().Where(x => x.name.ToLower().Contains(name.ToLower())).OrderBy(x => x.name).ToList();

            }
            else
            {
                bdParts = list.ToConvertObjects<List<CustomPart>>().OrderBy(x => x.name).ToList();

            }
            bodyparts.DataSource = bdParts;
            bodyparts.DisplayMember = "fullName";
            bodyparts.ValueMember = "partId";


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            FiltredBodyPart(textBox1.Text);
        }

        private async void bodyparts_SelectedIndexChangedAsync(object sender, EventArgs e)
        {

        }

        private void bodyparts_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //if (e.NewValue == CheckState.Checked && bodyparts.CheckedItems.Count >= 4)
            //    e.NewValue = CheckState.Unchecked;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            //((TextBox)sender).Text = string.Format("{0:#,##0.00}", double.Parse(((TextBox)sender).Text));

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private async void Beast_CheckedChangedAsync(object sender, EventArgs e)
        {
            var obg = ((CheckBox)sender);
            if (obg.Checked)
            {
                Request.variables.criteria.classes.Add(obg.Text);
            }
            else
            {
                Request.variables.criteria.classes.Remove(obg.Text);
            }
            // await Filtred();
        }

        private void _breed01_ValueChanged(object sender, EventArgs e)
        {

        }

        private async void _breed01_LeaveAsync(object sender, EventArgs e)
        {
            // await Filtred();

        }

        private void result_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void result_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (result.CurrentRow != null && result.CurrentRow.DataBoundItem != null)
                {
                    var current = ((Result)result.CurrentRow.DataBoundItem);
                    currentaxie.Image = current.realImage;
                    System.Diagnostics.Process.Start(current.url);

                }

            }
            catch { }
        }
        bool moreles(string priceone, string pricetwo, string pricetree, string pricefour)
        {


            var n1 = decimal.Parse(priceone, CultureInfo.InvariantCulture);
            var n2 = decimal.Parse(pricetwo, CultureInfo.InvariantCulture);
            var n3 = decimal.Parse(pricetree, CultureInfo.InvariantCulture);
            var n4 = decimal.Parse(pricefour, CultureInfo.InvariantCulture);


            var A = n1 >= n2;
            var B = n3 <= n4;

            return A && B;

        }
        private async void timer1_TickAsync(object sender, EventArgs e)
        {


            if (AxieNotifiqued == null)
            {
                AxieNotifiqued = new List<AxieResult>();
            }
            if (!string.IsNullOrEmpty(_fromusd01.Text) && !string.IsNullOrEmpty(_tosd01.Text))
            {

                try
                {
                    var tes = await _procees.GetAxieFiltred((Request.ToDictionary()));

                    if (tes != null)
                    {
                        result.DataSource = tes.data.axies.results;

                        var unique = tes.data.axies.results.FirstOrDefault();
                        var exi = tes.data.axies.results.FirstOrDefault(x => moreles(x.price, _fromusd01.Text, x.price, _tosd01.Text));
                        if (exi != null)
                        {

                            var inter = AxieNotifiqued.FirstOrDefault(x => x.id == exi.id);


                            AxieNotifiqued.Add(unique.ToConvertObjects<AxieResult>());

                            if (unique != null)
                            {

                                if (inter == null)
                                {
                                    this.Alert($"AXIE  {unique.id} FOUND ", Form_Alert.enmType.Success, unique, true);
                                    AddNotification(unique, exi, Color.DeepSkyBlue, "");

                                }
                                else
                                {
                                    if (decimal.Parse(unique.price, CultureInfo.InvariantCulture) > decimal.Parse(inter.price, CultureInfo.InvariantCulture))
                                    {
                                        this.Alert($"AXIE {unique.id} FOUND - PRICE UP ", Form_Alert.enmType.Error, unique, true);
                                        inter.price = unique.price;

                                        AddNotification(unique, exi, Color.Red, "PRICE UP");
                                    }
                                    if (decimal.Parse(unique.price, CultureInfo.InvariantCulture) < decimal.Parse(inter.price, CultureInfo.InvariantCulture))
                                    {
                                        inter.price = unique.price;
                                        this.Alert($"AXIE  {unique.id} FOUND - PRICE DOWN", Form_Alert.enmType.Warning, unique, true);
                                        AddNotification(unique, exi, Color.Orange, "PRICE DOWN");

                                    }
                                }


                            }
                        }
                    }
                }
                catch
                {

                }

            }

        }

        private void AddNotification(Result unique, Result exi, Color color, string txt = "")
        {
            AxieNotifications axicr = new AxieNotifications();
            axicr.Axie = exi;
            axicr.PictureAxie = exi.realImage;
            axicr.TextNotification = $"AXIE FOUND {unique.id} {txt}  PRICE {unique.price} USD";
            axicr.Time = DateTime.Now.ToString("dd/MM/yyyyy hh:mm");
            axicr.SendToBack();
            axicr.Dock = DockStyle.Top;
            axicr.Clickmio += Axicr_Clickmio;
            axicr.CloseNotification += Axicr_CloseNotification;
            axicr.ColorBG = color;
            axienotifications.Controls.Add(axicr);
        }

        private void Axicr_CloseNotification(AxieNotifications me)
        {
            me.Dispose();
        }

        private void Axicr_Clickmio(Result axie)
        {
            OpenAxie(axie);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            Request = new RequestFiltresViewModel();
            initobjaxie();

            Request.variables.criteria.parts.Clear();
            FiltredBodyPart(textBox1.Text);
            selected.Items.Clear();

        }



        private async void button3_ClickAsync(object sender, EventArgs e)
        {
            await Filtred();

        }

        bool active;
        private void button1_Click(object sender, EventArgs e)
        {

            if (active)
            {
                active = false;
                timer1.Enabled = false;
                timer1.Stop();
                Alarm.Text = "START ALARM";
                Alarm.BackColor = Color.DeepSkyBlue;
                this.Alert($"ALARM STOP", Form_Alert.enmType.Warning, null, false);

            }
            else
            {
                active = true;
                timer1.Enabled = true;
                timer1.Start();
                Alarm.Text = "STOP ALARM";
                Alarm.BackColor = Color.Orange;
                var save = Request.ToConvertObjects<RequestCustomFiltresViewModel>();
                save.pricemax = _tosd01.Text;
                save.pricemin = _fromusd01.Text;
                System.IO.File.WriteAllText($"Data/filtred{number}.json".GetFilePath(), save.ToStringJson());

                this.Alert($"ALARM IS RUN ", Form_Alert.enmType.Success, null, false);

            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.dudiver.com");
        }

        private void bodyparts_DoubleClick(object sender, EventArgs e)
        {

            if (selected.Items.Count < 4)
            {
                var item = bodyparts.SelectedValue;
                if (!Request.variables.criteria.parts.Contains(item.ToString()))
                {
                    Request.variables.criteria.parts.Add(item.ToString());
                    selected.Items.Add(item.ToString());
                }
            }
        }

        private void bodyparts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
