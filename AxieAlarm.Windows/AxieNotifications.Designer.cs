﻿
namespace AxieAlarm.Windows
{
    partial class AxieNotifications
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMsg = new System.Windows.Forms.Label();
            this.Axiepicture = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lbltime = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Axiepicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMsg
            // 
            this.lblMsg.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.White;
            this.lblMsg.Location = new System.Drawing.Point(89, 0);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(317, 83);
            this.lblMsg.TabIndex = 6;
            this.lblMsg.Text = "Message Text";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Click += new System.EventHandler(this.Axiepicture_Click);
            // 
            // Axiepicture
            // 
            this.Axiepicture.BackColor = System.Drawing.Color.Transparent;
            this.Axiepicture.Dock = System.Windows.Forms.DockStyle.Left;
            this.Axiepicture.Location = new System.Drawing.Point(0, 0);
            this.Axiepicture.Name = "Axiepicture";
            this.Axiepicture.Size = new System.Drawing.Size(89, 83);
            this.Axiepicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Axiepicture.TabIndex = 5;
            this.Axiepicture.TabStop = false;
            this.Axiepicture.Click += new System.EventHandler(this.Axiepicture_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = global::AxieAlarm.Windows.Properties.Resources.icons8_cancel_25px1;
            this.pictureBox2.Location = new System.Drawing.Point(506, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(44, 30);
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // lbltime
            // 
            this.lbltime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbltime.ForeColor = System.Drawing.Color.Black;
            this.lbltime.Location = new System.Drawing.Point(412, 3);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(106, 75);
            this.lbltime.TabIndex = 8;
            this.lbltime.Text = "{lbltime}";
            this.lbltime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AxieNotifications
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.Axiepicture);
            this.Name = "AxieNotifications";
            this.Size = new System.Drawing.Size(553, 83);
            this.Click += new System.EventHandler(this.Axiepicture_Click);
            ((System.ComponentModel.ISupportInitialize)(this.Axiepicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox Axiepicture;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lbltime;
    }
}
