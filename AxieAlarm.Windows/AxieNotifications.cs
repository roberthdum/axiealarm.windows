﻿using AxieAlarm.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AxieAlarm.Windows
{
    public partial class AxieNotifications : UserControl
    {
        public AxieNotifications()
        {
            InitializeComponent();
        }

        public string Time
        {
            get
            {
                return lbltime.Text;
            }
            set
            {
                lbltime.Text = value;
            }
        }
        public string TextNotification
        {
            get
            {
                return lblMsg.Text;
            }
            set
            {
                lblMsg.Text = value;
            }
        }
        public Image PictureAxie
        {
            get
            {
                return Axiepicture.Image;
            }
            set
            {
                Axiepicture.Image = value;
            }
        }
        public Color ColorBG
        {
            get
            {
                return this.BackColor;
            }
            set
            {
                this.BackColor = value;
            }
        }
        public Result Axie { get; set; }
        private void Axiepicture_Click(object sender, EventArgs e)
        {
            if (Clickmio != null)
            {
                Clickmio(Axie);
            }
        }
        public delegate void EventHandlerClick(Result axie);
        public delegate void EventHandler(AxieNotifications me);

        public event EventHandlerClick Clickmio;
        public event EventHandler CloseNotification;
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (CloseNotification != null)
            {
                CloseNotification(this);
            }
        }
    }
}
