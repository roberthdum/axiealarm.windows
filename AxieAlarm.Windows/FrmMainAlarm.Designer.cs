﻿
namespace AxieAlarm.Windows
{
    partial class AxieAlarm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AxieAlarm));
            this.result = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nameaxie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Image = new System.Windows.Forms.DataGridViewImageColumn();
            this.Url = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._tosd01 = new System.Windows.Forms.TextBox();
            this._fromusd01 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._purenes02 = new System.Windows.Forms.NumericUpDown();
            this._purenes01 = new System.Windows.Forms.NumericUpDown();
            this._breed02 = new System.Windows.Forms.NumericUpDown();
            this._breed01 = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.selected = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.currentaxie = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Dawn = new System.Windows.Forms.CheckBox();
            this.Reptile = new System.Windows.Forms.CheckBox();
            this.Bird = new System.Windows.Forms.CheckBox();
            this.Aquatic = new System.Windows.Forms.CheckBox();
            this.Dusk = new System.Windows.Forms.CheckBox();
            this.Mech = new System.Windows.Forms.CheckBox();
            this.Bug = new System.Windows.Forms.CheckBox();
            this.Plant = new System.Windows.Forms.CheckBox();
            this.Beast = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this._morale02 = new System.Windows.Forms.NumericUpDown();
            this._morale01 = new System.Windows.Forms.NumericUpDown();
            this._skill02 = new System.Windows.Forms.NumericUpDown();
            this._skill01 = new System.Windows.Forms.NumericUpDown();
            this.Alarm = new System.Windows.Forms.Button();
            this._speed02 = new System.Windows.Forms.NumericUpDown();
            this._speed01 = new System.Windows.Forms.NumericUpDown();
            this._health02 = new System.Windows.Forms.NumericUpDown();
            this._health01 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.axienotifications = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.bodyparts = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.result)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._purenes02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._purenes01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._breed02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._breed01)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.currentaxie)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._morale02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._morale01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._skill02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._skill01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._speed02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._speed01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._health02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._health01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // result
            // 
            this.result.AllowUserToAddRows = false;
            this.result.AllowUserToDeleteRows = false;
            this.result.AllowUserToOrderColumns = true;
            this.result.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.result.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.result.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.result.ColumnHeadersHeight = 29;
            this.result.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.result.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Nameaxie,
            this.Price,
            this.Image,
            this.Url});
            this.result.Location = new System.Drawing.Point(795, 5);
            this.result.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.result.Name = "result";
            this.result.ReadOnly = true;
            this.result.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.result.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.result.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.result.ShowCellErrors = false;
            this.result.ShowCellToolTips = false;
            this.result.ShowEditingIcon = false;
            this.result.Size = new System.Drawing.Size(372, 273);
            this.result.TabIndex = 12;
            this.result.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.result_CellContentClick);
            this.result.DoubleClick += new System.EventHandler(this.result_DoubleClick);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "id";
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 6;
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ID.Width = 27;
            // 
            // Nameaxie
            // 
            this.Nameaxie.DataPropertyName = "name";
            this.Nameaxie.HeaderText = "Name";
            this.Nameaxie.MinimumWidth = 6;
            this.Nameaxie.Name = "Nameaxie";
            this.Nameaxie.ReadOnly = true;
            this.Nameaxie.Width = 74;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "price";
            this.Price.HeaderText = "Price";
            this.Price.MinimumWidth = 6;
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.Width = 69;
            // 
            // Image
            // 
            this.Image.DataPropertyName = "realImage";
            this.Image.HeaderText = "Image";
            this.Image.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Image.MinimumWidth = 6;
            this.Image.Name = "Image";
            this.Image.ReadOnly = true;
            this.Image.Width = 52;
            // 
            // Url
            // 
            this.Url.DataPropertyName = "Url";
            this.Url.HeaderText = "Url";
            this.Url.MinimumWidth = 6;
            this.Url.Name = "Url";
            this.Url.ReadOnly = true;
            this.Url.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Url.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Url.Width = 32;
            // 
            // timer1
            // 
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_TickAsync);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 104);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1189, 567);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.SteelBlue;
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.currentaxie);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.result);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1181, 538);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Axie Filtred";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.axienotifications);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1181, 521);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Notifications";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.BackgroundImage = global::AxieAlarm.Windows.Properties.Resources.bg1;
            this.groupBox5.Controls.Add(this._tosd01);
            this.groupBox5.Controls.Add(this._fromusd01);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(286, 366);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(253, 153);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "How to Pay in U$D ";
            // 
            // _tosd01
            // 
            this._tosd01.Location = new System.Drawing.Point(11, 117);
            this._tosd01.Margin = new System.Windows.Forms.Padding(4);
            this._tosd01.Name = "_tosd01";
            this._tosd01.Size = new System.Drawing.Size(132, 26);
            this._tosd01.TabIndex = 3;
            this._tosd01.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this._tosd01.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            // 
            // _fromusd01
            // 
            this._fromusd01.Location = new System.Drawing.Point(11, 57);
            this._fromusd01.Margin = new System.Windows.Forms.Padding(4);
            this._fromusd01.Name = "_fromusd01";
            this._fromusd01.Size = new System.Drawing.Size(132, 26);
            this._fromusd01.TabIndex = 2;
            this._fromusd01.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this._fromusd01.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 92);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 20);
            this.label9.TabIndex = 1;
            this.label9.Text = "To";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 32);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "From";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.BackgroundImage = global::AxieAlarm.Windows.Properties.Resources.bg1;
            this.groupBox4.Controls.Add(this._purenes02);
            this.groupBox4.Controls.Add(this._purenes01);
            this.groupBox4.Controls.Add(this._breed02);
            this.groupBox4.Controls.Add(this._breed01);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(286, 166);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(253, 203);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "General";
            // 
            // _purenes02
            // 
            this._purenes02.Location = new System.Drawing.Point(125, 116);
            this._purenes02.Margin = new System.Windows.Forms.Padding(4);
            this._purenes02.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this._purenes02.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this._purenes02.Name = "_purenes02";
            this._purenes02.Size = new System.Drawing.Size(88, 26);
            this._purenes02.TabIndex = 10;
            this._purenes02.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this._purenes02.Leave += new System.EventHandler(this._breed01_LeaveAsync);
            // 
            // _purenes01
            // 
            this._purenes01.Location = new System.Drawing.Point(11, 116);
            this._purenes01.Margin = new System.Windows.Forms.Padding(4);
            this._purenes01.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this._purenes01.Name = "_purenes01";
            this._purenes01.Size = new System.Drawing.Size(88, 26);
            this._purenes01.TabIndex = 9;
            this._purenes01.Leave += new System.EventHandler(this._breed01_LeaveAsync);
            // 
            // _breed02
            // 
            this._breed02.Location = new System.Drawing.Point(125, 57);
            this._breed02.Margin = new System.Windows.Forms.Padding(4);
            this._breed02.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this._breed02.Name = "_breed02";
            this._breed02.Size = new System.Drawing.Size(88, 26);
            this._breed02.TabIndex = 8;
            this._breed02.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this._breed02.Leave += new System.EventHandler(this._breed01_LeaveAsync);
            // 
            // _breed01
            // 
            this._breed01.Location = new System.Drawing.Point(11, 57);
            this._breed01.Margin = new System.Windows.Forms.Padding(4);
            this._breed01.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this._breed01.Name = "_breed01";
            this._breed01.Size = new System.Drawing.Size(88, 26);
            this._breed01.TabIndex = 7;
            this._breed01.ValueChanged += new System.EventHandler(this._breed01_ValueChanged);
            this._breed01.Leave += new System.EventHandler(this._breed01_LeaveAsync);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 91);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 20);
            this.label7.TabIndex = 5;
            this.label7.Text = "PURENESS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 26);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(143, 20);
            this.label8.TabIndex = 4;
            this.label8.Text = "BREED COUNT";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.BackgroundImage = global::AxieAlarm.Windows.Properties.Resources.bg1;
            this.groupBox2.Controls.Add(this.bodyparts);
            this.groupBox2.Controls.Add(this.selected);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(6, 5);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(281, 514);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Body Parts";
            // 
            // selected
            // 
            this.selected.Enabled = false;
            this.selected.FormattingEnabled = true;
            this.selected.ItemHeight = 20;
            this.selected.Location = new System.Drawing.Point(6, 342);
            this.selected.Margin = new System.Windows.Forms.Padding(4);
            this.selected.Name = "selected";
            this.selected.Size = new System.Drawing.Size(265, 144);
            this.selected.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(216, 26);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(59, 28);
            this.button2.TabIndex = 3;
            this.button2.Text = "X";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(7, 26);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(200, 26);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // currentaxie
            // 
            this.currentaxie.Location = new System.Drawing.Point(795, 318);
            this.currentaxie.Margin = new System.Windows.Forms.Padding(4);
            this.currentaxie.Name = "currentaxie";
            this.currentaxie.Size = new System.Drawing.Size(372, 200);
            this.currentaxie.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.currentaxie.TabIndex = 13;
            this.currentaxie.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BackgroundImage = global::AxieAlarm.Windows.Properties.Resources.bg1;
            this.groupBox1.Controls.Add(this.Dawn);
            this.groupBox1.Controls.Add(this.Reptile);
            this.groupBox1.Controls.Add(this.Bird);
            this.groupBox1.Controls.Add(this.Aquatic);
            this.groupBox1.Controls.Add(this.Dusk);
            this.groupBox1.Controls.Add(this.Mech);
            this.groupBox1.Controls.Add(this.Bug);
            this.groupBox1.Controls.Add(this.Plant);
            this.groupBox1.Controls.Add(this.Beast);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(286, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(253, 174);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Class";
            // 
            // Dawn
            // 
            this.Dawn.AutoSize = true;
            this.Dawn.Location = new System.Drawing.Point(109, 102);
            this.Dawn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Dawn.Name = "Dawn";
            this.Dawn.Size = new System.Drawing.Size(78, 24);
            this.Dawn.TabIndex = 0;
            this.Dawn.Text = "Dawn";
            this.Dawn.UseVisualStyleBackColor = true;
            this.Dawn.CheckedChanged += new System.EventHandler(this.Beast_CheckedChangedAsync);
            // 
            // Reptile
            // 
            this.Reptile.AutoSize = true;
            this.Reptile.Location = new System.Drawing.Point(109, 75);
            this.Reptile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Reptile.Name = "Reptile";
            this.Reptile.Size = new System.Drawing.Size(90, 24);
            this.Reptile.TabIndex = 0;
            this.Reptile.Text = "Reptile";
            this.Reptile.UseVisualStyleBackColor = true;
            this.Reptile.CheckedChanged += new System.EventHandler(this.Beast_CheckedChangedAsync);
            // 
            // Bird
            // 
            this.Bird.AutoSize = true;
            this.Bird.Location = new System.Drawing.Point(109, 48);
            this.Bird.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Bird.Name = "Bird";
            this.Bird.Size = new System.Drawing.Size(66, 24);
            this.Bird.TabIndex = 0;
            this.Bird.Text = "Bird";
            this.Bird.UseVisualStyleBackColor = true;
            this.Bird.CheckedChanged += new System.EventHandler(this.Beast_CheckedChangedAsync);
            // 
            // Aquatic
            // 
            this.Aquatic.AutoSize = true;
            this.Aquatic.Location = new System.Drawing.Point(109, 21);
            this.Aquatic.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Aquatic.Name = "Aquatic";
            this.Aquatic.Size = new System.Drawing.Size(94, 24);
            this.Aquatic.TabIndex = 0;
            this.Aquatic.Text = "Aquatic";
            this.Aquatic.UseVisualStyleBackColor = true;
            this.Aquatic.CheckedChanged += new System.EventHandler(this.Beast_CheckedChangedAsync);
            // 
            // Dusk
            // 
            this.Dusk.AutoSize = true;
            this.Dusk.Location = new System.Drawing.Point(5, 129);
            this.Dusk.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Dusk.Name = "Dusk";
            this.Dusk.Size = new System.Drawing.Size(74, 24);
            this.Dusk.TabIndex = 0;
            this.Dusk.Text = "Dusk";
            this.Dusk.UseVisualStyleBackColor = true;
            this.Dusk.CheckedChanged += new System.EventHandler(this.Beast_CheckedChangedAsync);
            // 
            // Mech
            // 
            this.Mech.AutoSize = true;
            this.Mech.Location = new System.Drawing.Point(5, 102);
            this.Mech.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Mech.Name = "Mech";
            this.Mech.Size = new System.Drawing.Size(76, 24);
            this.Mech.TabIndex = 0;
            this.Mech.Text = "Mech";
            this.Mech.UseVisualStyleBackColor = true;
            this.Mech.CheckedChanged += new System.EventHandler(this.Beast_CheckedChangedAsync);
            // 
            // Bug
            // 
            this.Bug.AutoSize = true;
            this.Bug.Location = new System.Drawing.Point(5, 75);
            this.Bug.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Bug.Name = "Bug";
            this.Bug.Size = new System.Drawing.Size(64, 24);
            this.Bug.TabIndex = 0;
            this.Bug.Text = "Bug";
            this.Bug.UseVisualStyleBackColor = true;
            this.Bug.CheckedChanged += new System.EventHandler(this.Beast_CheckedChangedAsync);
            // 
            // Plant
            // 
            this.Plant.AutoSize = true;
            this.Plant.Location = new System.Drawing.Point(5, 48);
            this.Plant.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Plant.Name = "Plant";
            this.Plant.Size = new System.Drawing.Size(74, 24);
            this.Plant.TabIndex = 0;
            this.Plant.Text = "Plant";
            this.Plant.UseVisualStyleBackColor = true;
            this.Plant.CheckedChanged += new System.EventHandler(this.Beast_CheckedChangedAsync);
            // 
            // Beast
            // 
            this.Beast.AutoSize = true;
            this.Beast.Location = new System.Drawing.Point(5, 21);
            this.Beast.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Beast.Name = "Beast";
            this.Beast.Size = new System.Drawing.Size(80, 24);
            this.Beast.TabIndex = 0;
            this.Beast.Text = "Beast";
            this.Beast.UseVisualStyleBackColor = true;
            this.Beast.CheckedChanged += new System.EventHandler(this.Beast_CheckedChangedAsync);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.BackgroundImage = global::AxieAlarm.Windows.Properties.Resources.bg1;
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this._morale02);
            this.groupBox3.Controls.Add(this._morale01);
            this.groupBox3.Controls.Add(this._skill02);
            this.groupBox3.Controls.Add(this._skill01);
            this.groupBox3.Controls.Add(this.Alarm);
            this.groupBox3.Controls.Add(this._speed02);
            this.groupBox3.Controls.Add(this._speed01);
            this.groupBox3.Controls.Add(this._health02);
            this.groupBox3.Controls.Add(this._health01);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(535, 5);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(253, 514);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Stats";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.button3.Location = new System.Drawing.Point(5, 279);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(88, 42);
            this.button3.TabIndex = 13;
            this.button3.Text = "FIltred";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_ClickAsync);
            // 
            // _morale02
            // 
            this._morale02.Location = new System.Drawing.Point(125, 245);
            this._morale02.Margin = new System.Windows.Forms.Padding(4);
            this._morale02.Maximum = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this._morale02.Minimum = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this._morale02.Name = "_morale02";
            this._morale02.Size = new System.Drawing.Size(88, 26);
            this._morale02.TabIndex = 12;
            this._morale02.Value = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this._morale02.Leave += new System.EventHandler(this._breed01_LeaveAsync);
            // 
            // _morale01
            // 
            this._morale01.Location = new System.Drawing.Point(11, 245);
            this._morale01.Margin = new System.Windows.Forms.Padding(4);
            this._morale01.Maximum = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this._morale01.Minimum = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this._morale01.Name = "_morale01";
            this._morale01.Size = new System.Drawing.Size(88, 26);
            this._morale01.TabIndex = 11;
            this._morale01.Value = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this._morale01.Leave += new System.EventHandler(this._breed01_LeaveAsync);
            // 
            // _skill02
            // 
            this._skill02.Location = new System.Drawing.Point(125, 182);
            this._skill02.Margin = new System.Windows.Forms.Padding(4);
            this._skill02.Maximum = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this._skill02.Minimum = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this._skill02.Name = "_skill02";
            this._skill02.Size = new System.Drawing.Size(88, 26);
            this._skill02.TabIndex = 10;
            this._skill02.Value = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this._skill02.Leave += new System.EventHandler(this._breed01_LeaveAsync);
            // 
            // _skill01
            // 
            this._skill01.Location = new System.Drawing.Point(11, 182);
            this._skill01.Margin = new System.Windows.Forms.Padding(4);
            this._skill01.Maximum = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this._skill01.Minimum = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this._skill01.Name = "_skill01";
            this._skill01.Size = new System.Drawing.Size(88, 26);
            this._skill01.TabIndex = 9;
            this._skill01.Value = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this._skill01.Leave += new System.EventHandler(this._breed01_LeaveAsync);
            // 
            // Alarm
            // 
            this.Alarm.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Alarm.Location = new System.Drawing.Point(97, 279);
            this.Alarm.Margin = new System.Windows.Forms.Padding(4);
            this.Alarm.Name = "Alarm";
            this.Alarm.Size = new System.Drawing.Size(149, 42);
            this.Alarm.TabIndex = 4;
            this.Alarm.Text = "START ALARM";
            this.Alarm.UseVisualStyleBackColor = false;
            this.Alarm.Click += new System.EventHandler(this.button1_Click);
            // 
            // _speed02
            // 
            this._speed02.Location = new System.Drawing.Point(125, 116);
            this._speed02.Margin = new System.Windows.Forms.Padding(4);
            this._speed02.Maximum = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this._speed02.Minimum = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this._speed02.Name = "_speed02";
            this._speed02.Size = new System.Drawing.Size(88, 26);
            this._speed02.TabIndex = 10;
            this._speed02.Value = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this._speed02.Leave += new System.EventHandler(this._breed01_LeaveAsync);
            // 
            // _speed01
            // 
            this._speed01.Location = new System.Drawing.Point(11, 116);
            this._speed01.Margin = new System.Windows.Forms.Padding(4);
            this._speed01.Maximum = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this._speed01.Minimum = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this._speed01.Name = "_speed01";
            this._speed01.Size = new System.Drawing.Size(88, 26);
            this._speed01.TabIndex = 9;
            this._speed01.Value = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this._speed01.Leave += new System.EventHandler(this._breed01_LeaveAsync);
            // 
            // _health02
            // 
            this._health02.Location = new System.Drawing.Point(125, 57);
            this._health02.Margin = new System.Windows.Forms.Padding(4);
            this._health02.Maximum = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this._health02.Minimum = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this._health02.Name = "_health02";
            this._health02.Size = new System.Drawing.Size(88, 26);
            this._health02.TabIndex = 8;
            this._health02.Value = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this._health02.Leave += new System.EventHandler(this._breed01_LeaveAsync);
            // 
            // _health01
            // 
            this._health01.Location = new System.Drawing.Point(11, 57);
            this._health01.Margin = new System.Windows.Forms.Padding(4);
            this._health01.Maximum = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this._health01.Minimum = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this._health01.Name = "_health01";
            this._health01.Size = new System.Drawing.Size(88, 26);
            this._health01.TabIndex = 7;
            this._health01.Value = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this._health01.Leave += new System.EventHandler(this._breed01_LeaveAsync);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 220);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "MORALE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 153);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "SKILL";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 91);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "SPEED";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "HEALTH";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1189, 104);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // axienotifications
            // 
            this.axienotifications.AutoScroll = true;
            this.axienotifications.BackColor = System.Drawing.Color.LightBlue;
            this.axienotifications.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.axienotifications.Location = new System.Drawing.Point(8, 6);
            this.axienotifications.Name = "axienotifications";
            this.axienotifications.Size = new System.Drawing.Size(678, 464);
            this.axienotifications.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(736, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(423, 36);
            this.label5.TabIndex = 1;
            this.label5.Text = "Developed by Roberth Dudiver";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.ForeColor = System.Drawing.Color.White;
            this.linkLabel1.LinkColor = System.Drawing.Color.White;
            this.linkLabel1.Location = new System.Drawing.Point(844, 45);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(251, 36);
            this.linkLabel1.TabIndex = 2;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "www.dudiver.com";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // bodyparts
            // 
            this.bodyparts.FormattingEnabled = true;
            this.bodyparts.ItemHeight = 20;
            this.bodyparts.Location = new System.Drawing.Point(6, 57);
            this.bodyparts.Name = "bodyparts";
            this.bodyparts.Size = new System.Drawing.Size(266, 284);
            this.bodyparts.TabIndex = 5;
            this.bodyparts.SelectedIndexChanged += new System.EventHandler(this.bodyparts_SelectedIndexChanged);
            this.bodyparts.DoubleClick += new System.EventHandler(this.bodyparts_DoubleClick);
            // 
            // AxieAlarm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(1189, 671);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "AxieAlarm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Axie Infinity Alarm";
            this.Load += new System.EventHandler(this.Form1_LoadAsync);
            ((System.ComponentModel.ISupportInitialize)(this.result)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._purenes02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._purenes01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._breed02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._breed01)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.currentaxie)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._morale02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._morale01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._skill02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._skill01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._speed02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._speed01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._health02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._health01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox Dawn;
        private System.Windows.Forms.CheckBox Reptile;
        private System.Windows.Forms.CheckBox Bird;
        private System.Windows.Forms.CheckBox Aquatic;
        private System.Windows.Forms.CheckBox Dusk;
        private System.Windows.Forms.CheckBox Mech;
        private System.Windows.Forms.CheckBox Bug;
        private System.Windows.Forms.CheckBox Plant;
        private System.Windows.Forms.CheckBox Beast;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown _morale02;
        private System.Windows.Forms.NumericUpDown _morale01;
        private System.Windows.Forms.NumericUpDown _skill02;
        private System.Windows.Forms.NumericUpDown _skill01;
        private System.Windows.Forms.NumericUpDown _speed02;
        private System.Windows.Forms.NumericUpDown _speed01;
        private System.Windows.Forms.NumericUpDown _health02;
        private System.Windows.Forms.NumericUpDown _health01;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown _purenes02;
        private System.Windows.Forms.NumericUpDown _purenes01;
        private System.Windows.Forms.NumericUpDown _breed02;
        private System.Windows.Forms.NumericUpDown _breed01;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox _fromusd01;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _tosd01;
        private System.Windows.Forms.Button Alarm;
        private System.Windows.Forms.DataGridView result;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nameaxie;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewImageColumn Image;
        private System.Windows.Forms.DataGridViewTextBoxColumn Url;
        private System.Windows.Forms.PictureBox currentaxie;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox selected;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel axienotifications;
        private System.Windows.Forms.ListBox bodyparts;
    }
}

