﻿using AxieAlarm.ApiRest.Models;
using System.Collections.Generic;

namespace AxieAlarm.ApiRest
{
    public interface IConfigurationsEndpoints
    {
        Apiendpoint GetEndpoint(string Name);
        List<Apiendpoint> GetEndpoints();
    }
}