﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AxieAlarm.Configuration
{
    public class AppKeys
    {
        public const string Bearer = "Bearer";
        public const string SapBaseUrl = "SapBaseUrl";


        public const string Endpoints = "apiendpoints";
        public const string EndpointCustomerHashKey = "getCustomerHashKey";
        public const string EndpointBukeala = "bukeala";
        public const string EndpointDoc24 = "doc24";
        public const string BukealaUrl = "bukealaUrl";



        public const string Lated = "demora";

        public const string Doc24key = "doc24key";
        public struct JwTokenn
        {
            public const string TokenExpirationError = "TokenExpirationError";
            public const string TokenAuth = "TokenAuth";
            public const string MissingData = "MissingData";
            public const string SymmetricSecurityKey = "symmetricSecurityKey";
            public const string UserToken = "USERTOKEN";

        }

        public struct Logger
        {
            public const string ServerSeq = "logger:serverSeq";

            public struct File
            {
                public const string Name = "logger:file:name";
                public const string Extension = "logger:file:extension";
                public const string Directory = "logger:file:directory";
                public const string MaxSize = "logger:file:maxSize";
                public const string MaxRetainedFiles = "logger:file:maxRetainedFiles";
                public const string LogPipelineElapsedTime = "logger:file:logPipelineElapsedTime";

                public struct Source
                {
                    public struct Generic
                    {
                        public const string Name = "logger:file:source:generic:name";
                        public const string Extension = "logger:file:source:generic:extension";
                        public const string Directory = "logger:file:source:generic:directory";
                    }

                }
            }

        }
    }
}
