﻿using System;

namespace AxieAlarm.Configuration
{
    public class ConnectionStrings
    {
        public const string SqlServerConnection = "SqlServerConnection";
        public const string SqlServerConnectionApp = "SqlServerConnectionApp";
        public const string SqlServerLOG = "SqlServerLOG";

        public const string PostgresqlConnection = "PostgresqlConnection";
        public const string DatabaseEngine = "DatabaseEngine";
        public const string WithCache = "WithCache";
    }
}
