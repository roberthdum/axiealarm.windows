﻿namespace AxieAlarm.Data.Entities
{
    public class Stats
    {
        public int hp { get; set; }
        public int speed { get; set; }
        public int skill { get; set; }
        public int morale { get; set; }
        public string __typename { get; set; }
    }

}
