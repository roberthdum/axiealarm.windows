﻿namespace AxieAlarm.Data.Entities
{
    public class Ability
    {
        public string id { get; set; }
        public string name { get; set; }
        public int attack { get; set; }
        public int defense { get; set; }
        public int energy { get; set; }
        public string description { get; set; }
        public string backgroundUrl { get; set; }
        public string effectIconUrl { get; set; }
        public string __typename { get; set; }
    }

}
