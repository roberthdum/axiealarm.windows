﻿using Newtonsoft.Json;

namespace AxieAlarm.Data.Entities
{

    public class CustomPart: Part
    {
        public string fullName { get {

                return $"{type} - {name}";
            } }

    }
    public class Part
    {

        public string partId { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        [JsonProperty("class")]
        public string _class { get; set; }
        public string type { get; set; }
        public object specialGenes { get; set; }
        public int stage { get; set; }

        public Ability[] abilities { get; set; }
        [JsonProperty("typename")]

        public string __typename { get; set; }
    }

}
