﻿namespace AxieAlarm.Data.Entities
{
    public class Axies
    {
        public int total { get; set; }
        public Result[] results { get; set; }
        public string __typename { get; set; }
    }

}
