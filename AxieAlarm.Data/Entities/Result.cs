﻿using Newtonsoft.Json;
using System.Drawing;
using System.Net;

namespace AxieAlarm.Data.Entities
{
    public class AxieResult
    {
        public string id { get; set; }
        public string image { get; set; }
        public Bitmap realImage
        {
            get
            {

                try
                {
                    if (!string.IsNullOrEmpty(image))
                    {
                        HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(image);
                        myRequest.Method = "GET";
                        HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                        System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(myResponse.GetResponseStream());
                        myResponse.Close();

                        return bmp;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch { return null; }
            }
        }
        public string url
        {
            get

            {
                return $"https://marketplace.axieinfinity.com/axie/{id}";
            }
        }
        public string price { get; set; }
    }
        public class Result
    {
        public string id { get; set; }
        public string image { get; set; }
        public Bitmap realImage
        {
            get
            {

                try
                {
                    if (!string.IsNullOrEmpty(image))
                    {
                        HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(image);
                        myRequest.Method = "GET";
                        HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                        System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(myResponse.GetResponseStream());
                        myResponse.Close();

                        return bmp;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch { return null; }
            }
        }

        [JsonProperty("class")]

        public string _class { get; set; }
        public string name { get; set; }
        public string genes { get; set; }
        public string owner { get; set; }
        public int stage { get; set; }
        public string title { get; set; }
        public int breedCount { get; set; }
        public int level { get; set; }
        public string url
        {
            get

            {
                return $"https://marketplace.axieinfinity.com/axie/{id}";
            }
        }

        public Part[] parts { get; set; }
        public Stats stats { get; set; }
        public Auction auction { get; set; }

        public string price { get { return auction.currentPriceUSD; } }

        [JsonProperty("typename")]

        public string __typename { get; set; }
    }

}
