﻿namespace AxieAlarm.Data.Entities
{
    public class Auction
    {
        public string startingPrice { get; set; }
        public string endingPrice { get; set; }
        public string startingTimestamp { get; set; }
        public string endingTimestamp { get; set; }
        public string duration { get; set; }
        public string timeLeft { get; set; }
        public string currentPrice { get; set; }
        public string currentPriceUSD { get; set; }
        public string suggestedPrice { get; set; }
        public string seller { get; set; }
        public int listingIndex { get; set; }
        public string state { get; set; }
        public string __typename { get; set; }
    }

}
