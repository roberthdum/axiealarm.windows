﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxieAlarm.Data.ViewModels
{

    public class RequestCustomFiltresViewModel: RequestFiltresViewModel
    {
     

        public string pricemin { get; set; }


        public string pricemax { get; set; }

    }
    public class RequestFiltresViewModel
    {
        public string operationName { get; set; }
        public Variables variables { get; set; }
        public string query { get; set; }
    }

    public class Variables
    {
        public int from { get; set; }
        public int size { get; set; }
        public string sort { get; set; }
        public string auctionType { get; set; }
        public object owner { get; set; }
        public Criteria criteria { get; set; }
    }

    public class Criteria
    {
        public object region { get; set; }
        public List<string> parts { get; set; }
        public object bodyShapes { get; set; }
        public List<string> classes { get; set; }
        public object stages { get; set; }
        public object numMystic { get; set; }
        public List<int> pureness { get; set; }
        public object title { get; set; }
        public object breedable { get; set; }
        public List<int> breedCount { get; set; }
        public List<int> hp { get; set; }
        public List<int> skill { get; set; }
        public List<int> speed { get; set; }
        public List<int> morale { get; set; }
    }

}
