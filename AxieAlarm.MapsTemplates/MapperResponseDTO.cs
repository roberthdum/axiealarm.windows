﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxieAlarm.MapsTemplates
{
    public class MapperResponseDTO
    {
        public Dictionary<string, object> Data { get; set; }
        public Dictionary<string, object> UnMapped { get; set; }
    }
}
