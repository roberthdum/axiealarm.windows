﻿using System.Collections.Generic;

namespace AxieAlarm.MapsTemplates
{
    public interface ICustomMapping
    {
        MapperResponseDTO Map(Dictionary<string, object> data, MapJsonType type, bool onboaarding = false);
        MapperResponseDTO Map(Dictionary<string, object> data, string fileName, bool onboaarding = false);
        MapperResponseDTO Maptemplate(Dictionary<string, object> data, MapJsonType type);
        MapperResponseDTO Maptemplate(Dictionary<string, object> data, string fileName);
        string Maptemplates(Dictionary<string, object> data, MapJsonType fileName);

        T Maptemplates<T>(Dictionary<string, object> data, MapJsonType type);
    }
}