﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxieAlarm.MapsTemplates
{
    public struct CoreFieldTypes
    {
        public const string ParameterTypeRecord = "ParameterTypeRecord";
        public const string ObjectTypeRecord = "ObjectTypeRecord";
    }
}
