﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxieAlarm.MapsTemplates
{
    public class MapConfiguration
    {
        public string SourceField { get; set; }
        public string CoreField { get; set; }
        public string CoreFieldType { get; set; }
        public IList<MapConfiguration> Children { get; set; }
        public object DefaultValue { get; set; }
        public MappingFunctionDTO Function { get; set; }
    }
}
