﻿namespace AxieAlarm.MapsTemplates
{
    public class MappingFunctionDTO
    {
        public string FunctionName { get; set; }
        public object Parameter { get; set; }
    }
}
