﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using System.Reflection;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Text;
using AxieAlarm.Generic;

namespace AxieAlarm.MapsTemplates
{
    public class JsonCustomMapping : ICustomMapping
    {
        private static Dictionary<string, IEnumerable<MapConfiguration>> Mapping = new Dictionary<string, IEnumerable<MapConfiguration>>();


        public MapperResponseDTO Map(Dictionary<string, object> data, MapJsonType type, bool onboaarding = false)
        {
            return Map(data, type.ToString(), onboaarding);
        }

        public MapperResponseDTO Map(Dictionary<string, object> data, string fileName, bool onboaarding = false)
        {
            IEnumerable<MapConfiguration> maps = GetMapping(fileName);
            return ResolveMap(data, maps, onboaarding);
        }
        public MapperResponseDTO Maptemplate(Dictionary<string, object> data, MapJsonType type)
        {
            return Maptemplate(data, type.ToString());
        }

        public MapperResponseDTO Maptemplate(Dictionary<string, object> data, string fileName)
        {
            var template = GetFile(fileName);
            var mappper = new MapperResponseDTO();
            template = SetValueToByProprety(template, MapTypes.Default, data);
            template = SetValueToByProprety(template, MapTypes.Function, data);
            template = SetValueToByProprety(template, MapTypes.DynamicValue, data);
            mappper.Data = JsonConvert.DeserializeObject<Dictionary<string, object>>(template);
            return mappper;
        }
        public T Maptemplates<T>(Dictionary<string, object> data, MapJsonType type)
        {
            return Maptemplates(data, type.ToString()).ToConvertObjects<T>(); ;
        }
        public string Maptemplates(Dictionary<string, object> data, MapJsonType type)
        {
            return Maptemplates(data, type.ToString());
        }
        public string Maptemplates(Dictionary<string, object> data, string fileName)
        {
            var template = GetFile(fileName);
            var mappper = new MapperResponseDTO();
            template = SetValueToByProprety(template, MapTypes.Default, data);
            template = SetValueToByProprety(template, MapTypes.Function, data);
            template = SetValueToByProprety(template, MapTypes.DynamicValue, data);
            return template;
        }
        private string ConvertXmlToJson(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return JsonConvert.SerializeXmlNode(doc);
        }

        private string SetValueToByProprety(string text, MapTypes type, Dictionary<string, object> data)
        {
            var regular = "";
            switch (type)
            {
                case MapTypes.DynamicValue:
                    regular = @"\*_(.*?)_\*"; //*_value_*
                    break;
                case MapTypes.Function:
                    regular = @"\*\[(.*?)\]\*"; //*[value]*
                    break;
                default:
                    regular = @"\*\((.*?)\)\*"; // *(value)*
                    break;
            }

            var regex = new Regex(regular);
            var matches = regex.Matches(text);

            var textchange = text;
            var jsonobj = data.GetJsonObjetc();

            if (matches != null && matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    var valueWithoutBrackets = match.Groups[1].Value;
                    var valueWithBrackets = match.Value;
                    switch (type)
                    {
                        case MapTypes.DynamicValue:
                            var pasedstring = jsonobj.SelectToken(valueWithoutBrackets)?.Value<object>();

                            textchange = textchange.Replace(valueWithBrackets, pasedstring.ToStringJson());
                            break;
                        case MapTypes.Function:
                            MapConfiguration map = null;
                            if (!valueWithoutBrackets.StartsWith("<"))
                            {
                                if (valueWithoutBrackets.IndexOf(@"\") > -1)
                                {
                                    map = JsonConvert.DeserializeObject<MapConfiguration>(SanitizeReceivedJson(valueWithoutBrackets));
                                }
                                else
                                {
                                    map = JsonConvert.DeserializeObject<MapConfiguration>(valueWithoutBrackets);
                                }
                            }
                            else
                            {
                                var jsonvalue = ConvertXmlToJson(valueWithoutBrackets);
                                map = JsonConvert.DeserializeObject<MapConfiguration>(jsonvalue);
                            }
                            textchange = ChangeValue(data, textchange, valueWithBrackets, map, valueWithoutBrackets.IndexOf(@"\") > -1);

                            break;
                        default:
                            textchange = textchange.Replace(valueWithBrackets, GetPropValue(data, valueWithoutBrackets).ToString());
                            break;
                    }
                }
            }

            return textchange;
        }

        private string ChangeValue(Dictionary<string, object> data, string textchange, string valueWithBrackets, MapConfiguration map, bool hasjsondstring)
        {
            //var value = _functions(map.Function.FunctionName).Resolve(data, data, map.Function.Parameter);
            //valueWithBrackets = (hasjsondstring ? $@"\""{valueWithBrackets}\""" : valueWithBrackets);
            //textchange = textchange.Replace(valueWithBrackets, value.ToString());
            return textchange;
        }

        private string SanitizeReceivedJson(string uglyJson)
        {
            var sb = new StringBuilder(uglyJson);
            sb.Replace("\\\t", "\t");
            sb.Replace("\\\n", "\n");
            sb.Replace("\\\r", "\r");
            sb.Replace(@"\", "");

            return sb.ToString();
        }
        private object GetPropValue(Dictionary<string, object> src, string propName)
        {
            try
            {
                if (src.ContainsKey(propName))
                    if (src[propName] != null)
                    {
                        return src[propName];
                    }
                    else
                        return string.Empty;
                else
                    return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        private MapperResponseDTO ResolveMap(Dictionary<string, object> data, IEnumerable<MapConfiguration> maps, bool onboaarding = false)
        {
            var response = new Dictionary<string, object>();
            var keysToRemove = new List<string>();
            foreach (var map in maps)
            {
                object  value = null;
                if (map.SourceField != null)
                {
                    if (map.Children == null || map.Children.Count == 0)
                    {
                        var regex = @"((\w)*)(\[(\d)\]\.((\w)*))*";
                        var groups = Regex.Match(map.SourceField, regex);
                        var source = groups.Groups[1].Value;

                         data.TryGetValue(source, out value);
                        if (value is JArray)
                        {
                            var index = groups.Groups[4].Value;
                            var property = groups.Groups[5].Value;
                            if (!string.IsNullOrEmpty(index) && !string.IsNullOrEmpty(property) && ((JArray)value).Count > 0)
                            {
                                value = ((JValue)(((JArray)value).ElementAt(int.Parse(index))).Value<object>(property)).Value;
                            }
                        }
                    }
                    else
                    {
                        value = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> children = new List<Dictionary<string, object>>();
                        object tes = null;
                        data.TryGetValue(map.SourceField, out tes);
                        if (tes is JArray)
                            children = ((JArray)tes).ToObject<List<Dictionary<string, object>>>();
                        else
                            children = (List<Dictionary<string, object>>)tes;

                        if (children != null)
                        {
                            foreach (var dict in children)
                            {
                                var aux = ResolveChild(data, map, dict);

                                ((List<Dictionary<string, object>>)value).Add(aux);
                            }
                        }
                    }

                    keysToRemove.Add(map.SourceField);
                }
                else if (map.Children != null && map.Children.Count > 0)
                {
                    var aux = ResolveChild(data, map, data);

                    switch (map.CoreFieldType)
                    {
                        case CoreFieldTypes.ParameterTypeRecord:
                            value = aux;
                            break;
                        case CoreFieldTypes.ObjectTypeRecord:
                            value = new List<Dictionary<string, object>>() { aux };
                            break;
                    }
                }

                //if (map.Function != null)
                //{
                //    value = _functions(map.Function.FunctionName).Resolve(data, data, map.Function.Parameter);
                //}

                if (value == null && map.DefaultValue != null)
                    value = map.DefaultValue;
                if (onboaarding)
                {
                    response[map.CoreField] = value;
                }
                else
                {
                    response.Add(map.CoreField, value);
                    keysToRemove.ForEach(x => data.Remove(x));
                }
            }

            return new MapperResponseDTO
            {
                Data = response,
                UnMapped = data
            };
        }

        private Dictionary<string, object> ResolveChild(Dictionary<string, object> data, MapConfiguration map, Dictionary<string, object> subData)
        {
            var aux = new Dictionary<string, object>();

            foreach (var child in map.Children)
            {
                var value = (object)null;
                if (child.SourceField != null && subData.ContainsKey(child.SourceField))
                     subData.TryGetValue(child.SourceField, out value);
                else if (child.DefaultValue != null)
                    value = child.DefaultValue;
                //else if (child.Function != null)
                //    value = _functions(child.Function.FunctionName).Resolve(data, subData, child.Function.Parameter);

                aux.Add(child.CoreField, value);
            }

            return aux;
        }

        private IEnumerable<MapConfiguration> GetMapping(string name)
        {
            if (!Mapping.ContainsKey(name))
            {
                string json = GetFile(name);
                var maps = JsonConvert.DeserializeObject<IEnumerable<MapConfiguration>>(json);
                Mapping.Add(name, maps);
                return maps;
            }
            return Mapping[name];
        }

        private static string GetFile(string name)
        {
            var currentDirectory = Path.GetDirectoryName(Assembly.GetAssembly(typeof(JsonCustomMapping)).Locati‌​on);

            var json = File.ReadAllText(Path.Combine(currentDirectory, string.Format(@"Mapping/CustomMapping/{0}.txt", name)));
            return json;
        }
    }
}
