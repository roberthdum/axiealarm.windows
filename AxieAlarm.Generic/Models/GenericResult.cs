﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AxieAlarm.Generic.Models
{
    public class GenericResult
    {

        Dictionary<string, object> _data { get; set; }

        public Dictionary<string, object> data
        {

            get
            {

                if (_data == null)
                {
                    _data = new Dictionary<string, object>();
                }

                return _data;
            }
            set
            {
                _data = value;
            }
        }
        public string message { get; set; }
        public int ResponseCode { get; set; }

        public bool Error { get; set; }


    }
}
