﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AxieAlarm.Generic.Models
{
   public class Audit
    {
        public bool? Deleted { get; set; }
        [DefaultValue("false")]
        public DateTime DateCreated
        {
            get
            {
                if (dateCreated == null)
                {
                    dateCreated = DateTime.UtcNow;
                }
                return dateCreated;
            }

            set { this.dateCreated = value; }
        }

        private DateTime dateCreated;
    }
}
